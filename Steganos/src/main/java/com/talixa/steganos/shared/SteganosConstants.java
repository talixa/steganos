package com.talixa.steganos.shared;

public class SteganosConstants {
	
	public static final String APPNAME = "Steganos";
	public static final String VERSION = "2.2";
	
	public static final String ICON = "res/steganos.png";
	
	public static final String ICON_LOAD = "res/open.gif";
	public static final String ICON_SAVE = "res/save.gif";
	public static final String ICON_LOCK = "res/lock.gif";
	public static final String ICON_ZOOMIN = "res/zoomin.gif";
	public static final String ICON_ZOOMOUT = "res/zoomout.gif";
	
	public static final String TITLE_MAIN = APPNAME + " " + VERSION;
	public static final String TITLE_ABOUT = "About " + APPNAME;
	public static final String TITLE_OPEN_FILE = "Open File";
	public static final String TITLE_SAVE_FILE = "Save File";
	
	// file menu
	public static final String MENU_FILE = "File";
	public static final String MENU_OPEN = "Open";
	public static final String MENU_SAVE = "Save";
	public static final String MENU_EXPORT = "Export";
	public static final String MENU_EXIT = "Exit";
		
	// help menu
	public static final String MENU_HELP = "Help";	
	public static final String MENU_ABOUT = "About";		
	
	public static final String LABEL_OK = "Ok";
	public static final String LABEL_CANCEL = "Cancel";
	public static final String ENTER_MESSAGE = "Hidden Message: ";
	public static final String NO_FILE_SELECTED = "No File Selected";
	public static final String SELECT_OUTPUT_FILE = "Select Output File";
	public static final String SELECT_INPUT_FILE = "Select Input File";
	
	public static final String ERROR_TITLE = "Error";
	public static final String ERROR_FILE_LOAD = "Error loading image file";
	public static final String ERROR_FILE_NOIMG = "Not a valid image file";
	public static final String ERROR_FILE_SAVE = "Error saving image file";
	
	public static final int APP_WIDTH = 800;
	public static final int APP_HEIGHT = 600;
}
