package com.talixa.steganos.helpers;

public class StegoOptions {

	public enum Encoding {ASCII8, ASCII7, EBCIDIC};
	public enum BitInversion {ON, OFF, EVERY_OTHER};
	public enum BitReversal {ON, OFF};
	public enum BitOrder {HEIGHT_FIRST, WIDTH_FIRST};
	
	private Encoding encoding = Encoding.ASCII8;
	private BitInversion inversion = BitInversion.OFF;
	private BitReversal reversal = BitReversal.OFF;
	private BitOrder order = BitOrder.HEIGHT_FIRST;
	private int encodingBits = 8;
	
	public StegoOptions() {
		// DO NOTHING
	}
	
	public StegoOptions(Encoding encoding) {
		this.encoding = encoding;
		setEncodingBits();
	}
	
	public StegoOptions(Encoding encoding, BitInversion inversion, BitReversal reversal, BitOrder order) {
		this.encoding = encoding;
		this.inversion = inversion;
		this.reversal = reversal;
		this.order = order;
		setEncodingBits();
	}
	
	public int getEncodingBits() {
		return encodingBits;
	}

	public Encoding getEncoding() {
		return encoding;
	}

	public void setEncoding(Encoding encoding) {
		this.encoding = encoding;
		setEncodingBits();
	}

	public BitInversion getInversion() {
		return inversion;
	}

	public void setInversion(BitInversion inversion) {
		this.inversion = inversion;
	}

	public BitReversal getReversal() {
		return reversal;
	}

	public void setReversal(BitReversal reversal) {
		this.reversal = reversal;
	}
	
	public BitOrder getOrder() {
		return order;
	}

	public void setOrder(BitOrder order) {
		this.order = order;
	}

	private void setEncodingBits() {
		switch (encoding) {
			case ASCII8: encodingBits = 8; break;
			case ASCII7: encodingBits = 7; break;
			case EBCIDIC: encodingBits = 8; break;
		}
	}
}
