package com.talixa.steganos.helpers;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.talixa.steganos.helpers.StegoOptions.BitInversion;
import com.talixa.steganos.helpers.StegoOptions.BitReversal;
import com.talixa.steganos.helpers.StegoOptions.Encoding;

public class StegoEncoder {
	
	private static final int E2A[] = {
	          0,  1,  2,  3,156,  9,134,127,151,141,142, 11, 12, 13, 14, 15,
	         16, 17, 18, 19,157,133,  8,135, 24, 25,146,143, 28, 29, 30, 31,
	        128,129,130,131,132, 10, 23, 27,136,137,138,139,140,  5,  6,  7,
	        144,145, 22,147,148,149,150,  4,152,153,154,155, 20, 21,158, 26,
	         32,160,161,162,163,164,165,166,167,168, 91, 46, 60, 40, 43, 33,
	         38,169,170,171,172,173,174,175,176,177, 93, 36, 42, 41, 59, 94,
	         45, 47,178,179,180,181,182,183,184,185,124, 44, 37, 95, 62, 63,
	        186,187,188,189,190,191,192,193,194, 96, 58, 35, 64, 39, 61, 34,
	        195, 97, 98, 99,100,101,102,103,104,105,196,197,198,199,200,201,
	        202,106,107,108,109,110,111,112,113,114,203,204,205,206,207,208,
	        209,126,115,116,117,118,119,120,121,122,210,211,212,213,214,215,
	        216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,
	        123, 65, 66, 67, 68, 69, 70, 71, 72, 73,232,233,234,235,236,237,
	        125, 74, 75, 76, 77, 78, 79, 80, 81, 82,238,239,240,241,242,243,
	         92,159, 83, 84, 85, 86, 87, 88, 89, 90,244,245,246,247,248,249,
	         48, 49, 50, 51, 52, 53, 54, 55, 56, 57,250,251,252,253,254,255
	};
	
	private static final int A2E[] = {
	          0,  1,  2,  3, 55, 45, 46, 47, 22,  5, 37, 11, 12, 13, 14, 15,
	         16, 17, 18, 19, 60, 61, 50, 38, 24, 25, 63, 39, 28, 29, 30, 31,
	         64, 79,127,123, 91,108, 80,125, 77, 93, 92, 78,107, 96, 75, 97,
	        240,241,242,243,244,245,246,247,248,249,122, 94, 76,126,110,111,
	        124,193,194,195,196,197,198,199,200,201,209,210,211,212,213,214,
	        215,216,217,226,227,228,229,230,231,232,233, 74,224, 90, 95,109,
	        121,129,130,131,132,133,134,135,136,137,145,146,147,148,149,150,
	        151,152,153,162,163,164,165,166,167,168,169,192,106,208,161,  7,
	         32, 33, 34, 35, 36, 21,  6, 23, 40, 41, 42, 43, 44,  9, 10, 27,
	         48, 49, 26, 51, 52, 53, 54,  8, 56, 57, 58, 59,  4, 20, 62,225,
	         65, 66, 67, 68, 69, 70, 71, 72, 73, 81, 82, 83, 84, 85, 86, 87,
	         88, 89, 98, 99,100,101,102,103,104,105,112,113,114,115,116,117,
	        118,119,120,128,138,139,140,141,142,143,144,154,155,156,157,158,
	        159,160,170,171,172,173,174,175,176,177,178,179,180,181,182,183,
	        184,185,186,187,188,189,190,191,202,203,204,205,206,207,218,219,
	        220,221,222,223,234,235,236,237,238,239,250,251,252,253,254,255
	};
	
	private static int getXDimension(BufferedImage img, StegoOptions opts) {
		return opts.getOrder() == StegoOptions.BitOrder.HEIGHT_FIRST ? img.getWidth() : img.getHeight();
	}
	
	private static int getYDimension(BufferedImage img, StegoOptions opts) {
		return opts.getOrder() == StegoOptions.BitOrder.HEIGHT_FIRST ? img.getHeight() : img.getWidth();
	}
	
	private static int getBit(BufferedImage img, StegoOptions opts, int x, int y) {
		if (opts.getOrder() == StegoOptions.BitOrder.HEIGHT_FIRST) {
			return img.getRGB(x, y);
		} else {
			return img.getRGB(y, x);	
		}
	}
	
	private static void setBit(BufferedImage img, StegoOptions opts, int x, int y, int newVal) {
		if (opts.getOrder() == StegoOptions.BitOrder.HEIGHT_FIRST) {
			img.setRGB(x, y, newVal);
		} else {
			img.setRGB(y, x, newVal);	
		}
	}

	// Decode a single bit of data in each image pixel
	public static String retrieveStringFromImage(BufferedImage img, StegoOptions opts) {
		StringBuffer sb = new StringBuffer();
		int currBit = 0;						// current bit of output
		int currAscii = 0;						// current ascii char being generated
		boolean term = false;					// true when \0 termination encountered
		int encodingLength = opts.getEncodingBits();
		
		int xDimension = getXDimension(img, opts);
		int yDimension = getYDimension(img, opts);
		
		// for each pixel of the image - across & down
		for(int x = 0; x < xDimension && !term; ++x) {
			for(int y = 0; y < yDimension && !term; ++y) {
				
				// get pixel value for x/y location
				int value = getBit(img, opts, x, y);				
				// get the data bit from the pixel
				int bit = value & 0x01;
				if (opts.getInversion().equals(BitInversion.ON)) {
					bit = ~bit  & 0x01;
				} else if (opts.getInversion().equals(BitInversion.EVERY_OTHER) && currBit%2 == 0) {
					bit = ~bit  & 0x01;
				}
				
				// current ascii will be the previous ascii shifted left one ored with the current bit
				currAscii = (currAscii << 1) | bit;								
				
				// increment bit counter
				++currBit;
				
				// if we have collected 8 bits of data
				if (currBit % encodingLength == 0 && currBit > 0) {
					// check for null termination
					if (currAscii == 0) {
						term = true;
					}
					// append current char to string
					if (opts.getReversal().equals(BitReversal.ON)) {
						currAscii = reverseBits(currAscii, encodingLength);
					}
					
					if (!term) {	// null will appear as a square in Unix
						if (opts.getEncoding() == Encoding.EBCIDIC) {
							// convert to ascii if necessary
							sb.append((char)E2A[currAscii]);
						} else {
							sb.append((char)currAscii);
						}
					}
					// reset ascii char
					currAscii = 0;
				}
			}
		}
		
		// return data
		return sb.toString();
	}
	
	// encoding 1 data bit per pixel
	public static void createStegoImage(BufferedImage img, String message, String outputFile, StegoOptions opts) throws IOException {
		message += "\0";									// null terminate string
		int encodingLength = opts.getEncodingBits();		// bits in encoding scheme
		int bitCount = message.length() * encodingLength;	// total bits in message
		int currBit = 0;									// current bit of message
		
		int xDimension = getXDimension(img, opts);
		int yDimension = getYDimension(img, opts);
	
		// iterate through image pixels
		for(int x = 0; x < xDimension; ++x) {
			for(int y = 0; y < yDimension; ++y) {
				//get current pixel
				int value = getBit(img, opts, x, y);
				
				int newValue;
				// more bits to encode?
				if (currBit < bitCount) {
					// mask out last bit of pixel
					newValue = value & 0xFFFFFFFE;
					// get the current char we are encoding
					char charToEncode = message.charAt(currBit/encodingLength);
					
					// convert to ebcidic if necessary
					if (opts.getEncoding() == Encoding.EBCIDIC) {
						charToEncode = (char)A2E[charToEncode];
					}
					
					// reverse bits if necessary
					if (opts.getReversal().equals(BitReversal.ON)) {
						charToEncode = (char)reverseBits(charToEncode, encodingLength);
					}
					
					// get the bit of the char to encode
					int bitToEncode = (charToEncode >> ((encodingLength-1)-(currBit%encodingLength))) & 0x01;
					// or the encode bit with the masked pixel value
					if (opts.getInversion().equals(BitInversion.ON)) {
						bitToEncode = ~bitToEncode  & 0x01;
					} else if (opts.getInversion().equals(BitInversion.EVERY_OTHER) && currBit%2 == 0) {
						bitToEncode = ~bitToEncode  & 0x01;
					}
					newValue |= bitToEncode;
				} else {
					// nothing to encode - leave pixel alone
					newValue = value;					
				}
				
				// set new pixel value
				setBit(img, opts, x, y, newValue);
				
				// increment bit count
				++currBit;
			}
		}
		
		// write image
		ImageIO.write(img, "PNG", new File(outputFile));		
	}
	
	private static int reverseBits(int input, int encodingBits) {
		int reverse;
		if (encodingBits == 5) {
			reverse = (input & 0x01) << 4 |
				      (input & 0x02) << 2 |
				      (input & 0x04) << 0 |
				      (input & 0x08) >> 2 |
				      (input & 0x10) >> 4;
		} else if (encodingBits == 8) {
			reverse = (input & 0x01) << 7 |
					  (input & 0x02) << 5 |
					  (input & 0x04) << 3 |
					  (input & 0x08) << 1 |
					  (input & 0x10) >> 1 |
					  (input & 0x20) >> 3 |
					  (input & 0x40) >> 5 |
					  (input & 0x80) >> 7;
		} else if (encodingBits == 7) {
			reverse = (input & 0x01) << 6 |
					  (input & 0x02) << 4 |
					  (input & 0x04) << 2 |
					  (input & 0x08)      |
					  (input & 0x10) >> 2 |
					  (input & 0x20) >> 4 |
					  (input & 0x40) >> 6;
		} else {
			reverse = input;
		}
		return reverse;
	}
	
	public static void exportStegoData(BufferedImage img, StegoOptions opts, String exportFile) throws IOException {
		int bits[] = new int[img.getWidth() * img.getHeight()];
		
		int xDimension = getXDimension(img, opts);
		int yDimension = getYDimension(img, opts);
		int index = 0;

		for(int x = 0; x < xDimension; ++x) {
			for(int y = 0; y < yDimension; ++y) {
				// get pixel value for x/y location
				int value = getBit(img,opts,x,y);				
				// get the data bit from the pixel
				bits[index++] = value & 0x01;
			}
		}

		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(exportFile);
			for(int i = 0; i < index; i+=8) {
				int x = bits[i+0] << 7 |
						bits[i+1] << 6 |
						bits[i+2] << 5 |
						bits[i+3] << 4 |
						bits[i+4] << 3 |
						bits[i+5] << 2 |
						bits[i+6] << 1 |
						bits[i+7] << 0;
				fos.write(x);
			}
		} finally {
			if (fos != null) {
				fos.close();
			}
		}
	}
}
