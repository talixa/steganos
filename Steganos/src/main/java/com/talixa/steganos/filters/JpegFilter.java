package com.talixa.steganos.filters;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class JpegFilter extends FileFilter {
	@Override
    public boolean accept(File f){
        return f.getName().toLowerCase().endsWith(".jpg")||f.getName().toLowerCase().endsWith(".jpeg")||f.isDirectory();
    }
    @Override
    public String getDescription(){
        return "JPEG Images (*.jpg, *.jpeg)";
    }
}