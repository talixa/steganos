package com.talixa.steganos.filters;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class PngFilter extends FileFilter {
    @Override
    public boolean accept(File f){
        return f.getName().toLowerCase().endsWith(".png")||f.isDirectory();
    }
    @Override
    public String getDescription(){
        return "PNG Images (*.png)";
    }
}