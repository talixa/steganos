package com.talixa.steganos.filters;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class DataFilter extends FileFilter {
    @Override
    public boolean accept(File f){
        return f.getName().toLowerCase().endsWith(".dat")||f.isDirectory();
    }
    @Override
    public String getDescription(){
        return "Data Files (*.dat)";
    }
}
