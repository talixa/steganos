package com.talixa.steganos.filters;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class BitMapFilter extends FileFilter {
    @Override
    public boolean accept(File f){
        return f.getName().toLowerCase().endsWith(".bmp")||f.isDirectory();
    }
    @Override
    public String getDescription(){
        return "Bitmap Images (*.bmp)";
    }
}
