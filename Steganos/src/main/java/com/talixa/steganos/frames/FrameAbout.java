package com.talixa.steganos.frames;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.border.Border;

import com.talixa.steganos.listeners.ExitActionListener;
import com.talixa.steganos.shared.IconHelper;
import com.talixa.steganos.shared.SteganosConstants;

public class FrameAbout {
	
	private static final int WIDTH = 460;
	private static final int HEIGHT = 200;
	
	private static final String ABOUT_CONTENTS = 
			"<html><center>Steganos is a simple program to create and view messages embedded within image files." +
			"<br><br>Version: " + SteganosConstants.VERSION + 
			"<br>&copy;2017, Talixa Software & Service, LLC.</center></html>";
	
	private static final int BORDER = 10;
		
	public static void createAndShowGUI(JFrame owner) {
		// Create dialog
		JDialog dialog = new JDialog(owner, SteganosConstants.TITLE_ABOUT);
		dialog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		// set icon
		IconHelper.setIcon(dialog);		
		
		// set escape listener
		addEscapeListener(dialog);
		
		// set modality
		dialog.setModal(true);
		
		// create a content holder with a 10 pixel border
		JPanel holder = new JPanel();
		holder.setLayout(new BorderLayout());
		holder.setBorder(BorderFactory.createEmptyBorder(BORDER, BORDER, BORDER, BORDER));
		dialog.add(holder);
	
		// create components
		ClassLoader cl = IconHelper.class.getClassLoader();	
		JLabel icon = new JLabel(new ImageIcon(cl.getResource(SteganosConstants.ICON)));
		holder.add(icon, BorderLayout.WEST);
		
		JLabel text = new JLabel(ABOUT_CONTENTS);
		Border padding = BorderFactory.createEmptyBorder(BORDER, BORDER, BORDER, BORDER);
		text.setBorder(padding);
		JButton ok = new JButton(SteganosConstants.LABEL_OK);
		ok.addActionListener(new ExitActionListener(dialog));
				
		// Add to frame
		holder.add(text, BorderLayout.CENTER);
		holder.add(ok, BorderLayout.SOUTH);
				
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		dialog.setPreferredSize(new Dimension(WIDTH,HEIGHT));
		int left = (screenSize.width/2) - (WIDTH/2);
		int top  = (screenSize.height/2) - (HEIGHT/2);
		dialog.pack();
		dialog.setLocation(left,top);
		dialog.setVisible(true);						
	}	
	
	public static void addEscapeListener(final JDialog dialog) {
	    ActionListener escListener = new ActionListener() {
	        public void actionPerformed(ActionEvent e) {
	            dialog.dispose();
	        }
	    };

	    dialog.getRootPane().registerKeyboardAction(escListener,
	            KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
	            JComponent.WHEN_IN_FOCUSED_WINDOW);
	}
}
