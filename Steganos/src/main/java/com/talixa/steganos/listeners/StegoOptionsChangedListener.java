package com.talixa.steganos.listeners;

public interface StegoOptionsChangedListener {

	public void onOptionsChanged();
}
