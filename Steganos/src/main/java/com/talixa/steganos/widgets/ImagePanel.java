package com.talixa.steganos.widgets;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class ImagePanel extends JPanel {

	private BufferedImage original = null;
	private BufferedImage image = null;
	private float zoom = 1;

	public ImagePanel() {
    	// NOTHING
	}
    
	public void setImage(File image) throws IOException {
		try {
			this.original = ImageIO.read(image);
			this.image = original;
			repaint();
			
		} catch (IOException e) {
			this.image = null;
			throw e;
		}
	}
	
	public Dimension getPreferredSize() {
		return image == null ? super.getPreferredSize() : new Dimension(image.getWidth(), image.getHeight());
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (image != null) {
			g.drawImage(image, 0, 0, null);
		}
	}

	public BufferedImage getImage() {
		return image;
	}
	
	public void zoomOut() {
		zoom /= 2;
		showZoomedImage();
	}
	
	public void zoomIn() {
		zoom *= 2;
		showZoomedImage();
	}
	
	private void showZoomedImage() {
		if (original != null) {
			int newHeight = (int)(original.getHeight() * zoom);
			int newWidth = (int)(original.getWidth() * zoom);
			Image oimg = original.getScaledInstance(newWidth, newHeight, Image.SCALE_DEFAULT);
			image = convertToBufferedImage(oimg);
			repaint();
		}
	}
	
	private static BufferedImage convertToBufferedImage(Image img) {
	    if (img instanceof BufferedImage) {
	        return (BufferedImage) img;
	    }

	    // Create a buffered image with transparency
	    BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

	    // Draw the image on to the buffered image
	    Graphics2D bGr = bimage.createGraphics();
	    bGr.drawImage(img, 0, 0, null);
	    bGr.dispose();

	    // Return the buffered image
	    return bimage;
	}
}