package com.talixa.steganos.widgets;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import com.talixa.steganos.helpers.StegoOptions;
import com.talixa.steganos.helpers.StegoOptions.Encoding;
import com.talixa.steganos.listeners.StegoOptionsChangedListener;

@SuppressWarnings("serial")
public class OptionsPanel extends JPanel {

	// references to settings buttons
	private JRadioButton radioAscii8;
	private JRadioButton radioAscii7;
	private JRadioButton radioEbcidic;
	private JRadioButton radioInversionOff;
	private JRadioButton radioInversionOn;
	private JRadioButton radioInversionEveryOther;
	private JRadioButton radioReversalOff;
	private JRadioButton radioReversalOn;
	private JRadioButton radioOrderHeight;
	private JRadioButton radioOrderWidth;
	
	public OptionsPanel() {
		super();
				
		// encoding panel
		JPanel encodingPanel = new JPanel(new FlowLayout());
		JLabel encodingLabel = new JLabel("Encoding: ");
		radioAscii8 = new JRadioButton("ASCII 8");
		radioAscii7 = new JRadioButton("ASCII 7");
		radioEbcidic = new JRadioButton("EBCIDIC");
		ButtonGroup bgEncoding = new ButtonGroup();
		bgEncoding.add(radioAscii8);
		bgEncoding.add(radioAscii7);
		bgEncoding.add(radioEbcidic);
		bgEncoding.setSelected(radioAscii8.getModel(), true);
		encodingPanel.add(encodingLabel);
		encodingPanel.add(radioAscii8);
		encodingPanel.add(radioAscii7);
		encodingPanel.add(radioEbcidic);
		
		// inversion panel
		JPanel inversionPanel = new JPanel(new FlowLayout());
		JLabel inversionLabel = new JLabel("Inversion: ");
		radioInversionOff = new JRadioButton("Off");
		radioInversionOn = new JRadioButton("On");
		radioInversionEveryOther = new JRadioButton("Every Other Bit");
		ButtonGroup bgInversion = new ButtonGroup();
		bgInversion.add(radioInversionOff);
		bgInversion.add(radioInversionOn);
		bgInversion.add(radioInversionEveryOther);
		bgInversion.setSelected(radioInversionOff.getModel(), true);
		inversionPanel.add(inversionLabel);
		inversionPanel.add(radioInversionOff);
		inversionPanel.add(radioInversionOn);
		inversionPanel.add(radioInversionEveryOther);
		
		// reversal panel
		JPanel reversalPanel = new JPanel(new FlowLayout());
		JLabel reversalLabel = new JLabel("Reversal: ");
		radioReversalOff = new JRadioButton("Off");
		radioReversalOn = new JRadioButton("On");
		ButtonGroup bgReversal = new ButtonGroup();
		bgReversal.add(radioReversalOff);
		bgReversal.add(radioReversalOn);
		bgReversal.setSelected(radioReversalOff.getModel(), true);
		reversalPanel.add(reversalLabel);
		reversalPanel.add(radioReversalOff);
		reversalPanel.add(radioReversalOn);
		
		// order panel
		JPanel orderPanel = new JPanel(new FlowLayout());
		JLabel orderLabel = new JLabel("Order: ");
		radioOrderHeight = new JRadioButton("Column First");
		radioOrderWidth = new JRadioButton("Row First");
		ButtonGroup bgOrder= new ButtonGroup();
		bgOrder.add(radioOrderHeight);
		bgOrder.add(radioOrderWidth);
		bgOrder.setSelected(radioOrderHeight.getModel(), true);
		orderPanel.add(orderLabel);
		orderPanel.add(radioOrderHeight);
		orderPanel.add(radioOrderWidth);
			
		// put panels together
		JPanel mainPanel = new JPanel(new GridLayout(4, 1));
		mainPanel.add(orderPanel);
		mainPanel.add(inversionPanel);
		mainPanel.add(reversalPanel);
		mainPanel.add(encodingPanel);
		
		this.add(mainPanel);
	}
	
	public StegoOptions getSelectedOptions() {
		StegoOptions o = new StegoOptions();
		
		o.setReversal(radioReversalOn.isSelected() ? StegoOptions.BitReversal.ON : StegoOptions.BitReversal.OFF);
		o.setOrder(radioOrderHeight.isSelected() ? StegoOptions.BitOrder.HEIGHT_FIRST : StegoOptions.BitOrder.WIDTH_FIRST);
		
		if (radioAscii7.isSelected()) {
			o.setEncoding(Encoding.ASCII7);
		} else if (radioAscii8.isSelected()) {
			o.setEncoding(Encoding.ASCII8);
		} else {
			o.setEncoding(Encoding.EBCIDIC);
		}
		
		if (radioInversionOff.isSelected()) {
			o.setInversion(StegoOptions.BitInversion.OFF);
		} else if (radioInversionOn.isSelected()) {
			o.setInversion(StegoOptions.BitInversion.ON);
		} else {
			o.setInversion(StegoOptions.BitInversion.EVERY_OTHER);
		}
		
		return o;
	}
	
	public void setOptionsChangedListener(final StegoOptionsChangedListener optionsChangedListener) {
		JRadioButton[] allButtons = 
			{
			 radioAscii7, radioAscii8, radioEbcidic, 
			 radioInversionEveryOther, radioInversionOff, radioInversionOn, 
			 radioReversalOff, radioReversalOn,
			 radioOrderHeight, radioOrderWidth
			};
		
		ActionListener l = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				optionsChangedListener.onOptionsChanged();
			}
		};
		
		// set listener
		for(JRadioButton r : allButtons) {
			r.addActionListener(l);
		}
	}
}
