package com.talixa.steganos;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.filechooser.FileFilter;

import com.talixa.steganos.filters.BitMapFilter;
import com.talixa.steganos.filters.DataFilter;
import com.talixa.steganos.filters.JpegFilter;
import com.talixa.steganos.filters.PngFilter;
import com.talixa.steganos.frames.FrameAbout;
import com.talixa.steganos.helpers.StegoEncoder;
import com.talixa.steganos.helpers.StegoOptions;
import com.talixa.steganos.listeners.ExitActionListener;
import com.talixa.steganos.listeners.StegoOptionsChangedListener;
import com.talixa.steganos.shared.IconHelper;
import com.talixa.steganos.shared.SteganosConstants;
import com.talixa.steganos.widgets.ImagePanel;
import com.talixa.steganos.widgets.OptionsPanel;

public class SteganographyToolkit {

	private static JFileChooser fileChooser = new JFileChooser();
	private static JFrame frame;
	private static ImagePanel imagePanel;
	private static JScrollPane scrollPane;
	private static JTextArea lblText;
	private static OptionsPanel optionsPanel;
	private static JLabel statusText;
	
	private static boolean textLocked = false;
	private static String fileName = "No file loaded";
	
	private static void createAndShowGUI() {
		// Create frame
		frame = new JFrame(SteganosConstants.TITLE_MAIN);		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		
		// set icon
		IconHelper.setIcon(frame);		

		// create image panel
		imagePanel = new ImagePanel();	
		scrollPane = new JScrollPane(imagePanel);
		frame.add(scrollPane, BorderLayout.CENTER);	
		
		// panel on right is split in two - text and options
		JPanel rightPanel = new JPanel();
		rightPanel.setLayout(new BorderLayout());
		
		// top half is text
		lblText = new JTextArea(10,20);
		JScrollPane textPane = new JScrollPane(lblText);	
		lblText.setRows(10);
		lblText.setLineWrap(true);
		rightPanel.add(textPane, BorderLayout.CENTER);
		
		// botton half is options
		optionsPanel = new OptionsPanel();
		optionsPanel.setOptionsChangedListener(new StegoOptionsChangedListener() {
			@Override
			public void onOptionsChanged() {
				if (!textLocked) {
					BufferedImage i = imagePanel.getImage();
					if (i != null) {
						StegoOptions o = optionsPanel.getSelectedOptions();
						updateText(StegoEncoder.retrieveStringFromImage(i,o));
					}
				}
			}
		});
		rightPanel.add(optionsPanel, BorderLayout.SOUTH);
		
		// add right panel to frame
		frame.add(rightPanel, BorderLayout.EAST);
		
		// add status
		statusText = new JLabel();
		frame.add(statusText, BorderLayout.SOUTH);

		// Create menus and toolbar
		addMenus();		
		addToolbar();		
				
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setPreferredSize(new Dimension(SteganosConstants.APP_WIDTH,SteganosConstants.APP_HEIGHT));
		int left = (screenSize.width/2) - (SteganosConstants.APP_WIDTH/2);
		int top  = (screenSize.height/2) - (SteganosConstants.APP_HEIGHT/2);
		frame.pack();
		frame.setLocation(left,top);
		frame.setVisible(true);	
		
		// display status
		updateStatus();
	}	
	
	private static void addToolbar() {
		JToolBar toolbar = new JToolBar();
		
		// prevent the toolbar from being removable
		toolbar.setFloatable(false);
		
		// empty border for buttons
		Border emptyBorder = BorderFactory.createEmptyBorder(2,5,2,5);
		
		// Using images from: http://www.oracle.com/technetwork/java/index-138612.html
		JButton loadFile = new JButton(IconHelper.getImageIcon(SteganosConstants.ICON_LOAD));
		loadFile.setToolTipText("Load File");
		loadFile.setBorder(emptyBorder);
		loadFile.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				openFile();
			}
		});
		
		JButton saveFile = new JButton(IconHelper.getImageIcon(SteganosConstants.ICON_SAVE));
		saveFile.setToolTipText("Save File");
		saveFile.setBorder(emptyBorder);
		saveFile.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				saveFile();
			}
		});	
		
		JButton lockText = new JButton(IconHelper.getImageIcon(SteganosConstants.ICON_LOCK));
		lockText.setToolTipText("Lock Text");
		lockText.setBorder(emptyBorder);
		lockText.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				textLocked = !textLocked;
				updateStatus();
			}
		});
		
		JButton zoomin = new JButton(IconHelper.getImageIcon(SteganosConstants.ICON_ZOOMIN));
		zoomin.setToolTipText("Zoom In");
		zoomin.setBorder(emptyBorder);
		zoomin.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				imagePanel.zoomIn();
				updateScrollPane();
			}
		});
		
		JButton zoomout = new JButton(IconHelper.getImageIcon(SteganosConstants.ICON_ZOOMOUT));
		zoomout.setToolTipText("Zoom Out");
		zoomout.setBorder(emptyBorder);
		zoomout.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				imagePanel.zoomOut();
				updateScrollPane();
			}
		});
		
		toolbar.add(loadFile);
		toolbar.add(saveFile);
		toolbar.addSeparator();
		toolbar.add(zoomin);
		toolbar.add(zoomout);
		toolbar.addSeparator();
		toolbar.add(lockText);
		
		frame.add(toolbar, BorderLayout.NORTH);
	}
		
	private static void addMenus() {
		// Setup file menu
		JMenu fileMenu = new JMenu(SteganosConstants.MENU_FILE);
		fileMenu.setMnemonic(KeyEvent.VK_F);
		
		JMenuItem openMenuItem = new JMenuItem(SteganosConstants.MENU_OPEN);
		openMenuItem.setMnemonic(KeyEvent.VK_O);
		openMenuItem.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				openFile();
			}
		});
		fileMenu.add(openMenuItem);	
		
		JMenuItem saveMenuItem = new JMenuItem(SteganosConstants.MENU_SAVE);
		saveMenuItem.setMnemonic(KeyEvent.VK_S);
		saveMenuItem.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				saveFile();
			}
		});
		fileMenu.add(saveMenuItem);	
		
		fileMenu.addSeparator();
		
		JMenuItem exportMenuItem = new JMenuItem(SteganosConstants.MENU_EXPORT);
		exportMenuItem.setMnemonic(KeyEvent.VK_E);
		exportMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setFileFilters(FilterSet.EXPORT);
				if (fileChooser.showSaveDialog(frame) == JFileChooser.APPROVE_OPTION) {
					BufferedImage i = imagePanel.getImage();
					String out = fileChooser.getSelectedFile().getAbsolutePath();
					if (i != null) {
						StegoOptions o = optionsPanel.getSelectedOptions();
						try {
							StegoEncoder.exportStegoData(i, o, out);
						} catch (IOException e1) {
							showErrorMessage(SteganosConstants.ERROR_FILE_SAVE);
						}
					}
				}	
			}
		});
		fileMenu.add(exportMenuItem);
		fileMenu.addSeparator();
		
		JMenuItem exitMenuItem = new JMenuItem(SteganosConstants.MENU_EXIT);
		exitMenuItem.setMnemonic(KeyEvent.VK_X);
		exitMenuItem.addActionListener(new ExitActionListener(frame));
		fileMenu.add(exitMenuItem);			
		
		// Setup help menu
		JMenu helpMenu = new JMenu(SteganosConstants.MENU_HELP);
		helpMenu.setMnemonic(KeyEvent.VK_H);
		JMenuItem aboutMenuItem = new JMenuItem(SteganosConstants.MENU_ABOUT);
		aboutMenuItem.setMnemonic(KeyEvent.VK_A);
		aboutMenuItem.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				FrameAbout.createAndShowGUI(frame);
			}
		});
		helpMenu.add(aboutMenuItem);
		
		// Add menus to menubar
		JMenuBar menuBar = new JMenuBar();
		menuBar.add(fileMenu);	
		menuBar.add(helpMenu);
		frame.setJMenuBar(menuBar);
	}
	
	public static void showInfoMessage(String title, String msg) {
		JOptionPane.showMessageDialog(frame, msg, title, JOptionPane.INFORMATION_MESSAGE);
	}
	
	public static void showErrorMessage(String msg) {
		JOptionPane.showMessageDialog(frame, msg, SteganosConstants.ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
	}
	
	private static void updateStatus() {
		statusText.setText(" File: " + fileName + " - Text Status: " + (textLocked ? "Locked" : "Unlocked"));
	}
	
	private static void openFile() {
		// apply filter to file chooser
		setFileFilters(FilterSet.LOAD);
		
		if (fileChooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
			StegoOptions opts = optionsPanel.getSelectedOptions();
			try {	
				fileName = fileChooser.getSelectedFile().getName();
				
				updateImage(fileChooser.getSelectedFile());											
				updateText(StegoEncoder.retrieveStringFromImage(imagePanel.getImage(), opts));
				updateStatus();
			} catch (IOException e1) {
				showErrorMessage(SteganosConstants.ERROR_FILE_LOAD);
			} catch (NullPointerException npe) {
				showErrorMessage(SteganosConstants.ERROR_FILE_NOIMG);
			}
		}	
	}
	
	private static void updateText(String text) {
		lblText.setText(text);	
		lblText.setCaretPosition(0);
	}
	
	private static void updateImage(File image) throws IOException {
		imagePanel.setImage(image);	
		updateScrollPane();
	}
	
	private static void updateScrollPane() {
		scrollPane.setPreferredSize(imagePanel.getPreferredSize());
		scrollPane.updateUI();		// force scroll bars to appear if necessary		
	}
	
	private static void saveFile() {
		setFileFilters(FilterSet.SAVE);
		if (fileChooser.showSaveDialog(frame) == JFileChooser.APPROVE_OPTION) {
			BufferedImage img = imagePanel.getImage();
			String msg = lblText.getText();
			StegoOptions opts = optionsPanel.getSelectedOptions();
			String out = fileChooser.getSelectedFile().getAbsolutePath();
			try {
				StegoEncoder.createStegoImage(img, msg, out, opts);
			} catch (IOException e1) {
				showErrorMessage(SteganosConstants.ERROR_FILE_SAVE);
			}	
		}	
	}
	
	private enum FilterSet {LOAD, SAVE, EXPORT};
	
	private static FileFilter bitmapFilter = new BitMapFilter();
	private static FileFilter jpegFilter = new JpegFilter();
	private static FileFilter pngFilter = new PngFilter();
	private static FileFilter dataFilter = new DataFilter();
	
	private static void setFileFilters(FilterSet fs) {
		// clear all filters
		fileChooser.removeChoosableFileFilter(bitmapFilter);
		fileChooser.removeChoosableFileFilter(jpegFilter);
		fileChooser.removeChoosableFileFilter(pngFilter);
		fileChooser.removeChoosableFileFilter(dataFilter);
		
		if (fs == FilterSet.LOAD) {
			fileChooser.addChoosableFileFilter(bitmapFilter);
			fileChooser.addChoosableFileFilter(jpegFilter);
			fileChooser.addChoosableFileFilter(pngFilter);
		} else if (fs == FilterSet.SAVE) {
			fileChooser.setFileFilter(pngFilter);
		} else if (fs == FilterSet.EXPORT) {
			fileChooser.setFileFilter(dataFilter);
		}
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});				
	}	
}
