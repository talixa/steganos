# Java-based Steganography Tool #

A simple utility for retrieving text stored within images and to add embedded messages to pictures. Options include 7 or 8-bit ASCII as well as EBCIDIC encoding, bit inversion, and reversing of byte order.
